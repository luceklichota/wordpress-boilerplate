<?php
require_once (__DIR__.'/vendor/autoload.php');
define('WP_CONTENT_DIR', getenv('MAIN_DIR').'/content');
$_SERVER['REQUEST_URI'] = getenv('FOLDER').$_SERVER['REQUEST_URI'];

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv('DB_NAME') );

/** MySQL database username */
define( 'DB_USER', getenv('DB_USER') );

/** MySQL database password */
define( 'DB_PASSWORD', getenv('DB_PASSWORD') );

/** MySQL hostname */
define( 'DB_HOST', getenv('DB_HOST').':'.getenv('DB_PORT') );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';?=W.kNe-r*WX#-:v=E$}UB8$c*)ax[+9PBh_b=9%I)=qC]Di D(ev1^G`WWtrS?' );
define( 'SECURE_AUTH_KEY',  'yl}4.}/+LQ5%k?ZHe_KA=Hve)64ZfHLg^L44;C=NXo6|U:0J Q:oY@5CJfl.F}*6' );
define( 'LOGGED_IN_KEY',    'C>$Io%/Y++W;={f:{CR>pAC-IaF,OR^.>|ZHXo*UxGAY@b!$_fC3inYIin*E]ZsF' );
define( 'NONCE_KEY',        'vj~nJ4aPL,0Xp^9,D}M_,~vygUh-ds|Ey$4Q(Boq0E3@u,oS,Uxf,B/~PBvvl!kI' );
define( 'AUTH_SALT',        '%a(2]!n1GHT&#N~Ll,#Xk,+ou9K?iE~&x_6xhedNE]B~:EG@r]tRgOXEjkA4%Cu;' );
define( 'SECURE_AUTH_SALT', 'C>|#=`kW`7-L2|#u@]qNta9C%RAs%=4wJZ{7ZhPCG[HoOI2[%%c)Q+Sl1r0Z~oE`' );
define( 'LOGGED_IN_SALT',   '$A22fnEQ[~R_H[_cgwN7QLHx1):&ei<e>N>+sO`KQh:i!zvVMSdaz}7nG3lS7`Y;' );
define( 'NONCE_SALT',       'X{3#9iU~13<Cg0RapBIk9W59:lOyQ+@0wQ+_<H1a}Z<x[X  9wdSFZ _1X,I<5&x' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );


if (str_contains($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? '', 'https') || getenv('FORCE_SSL')) {
    $_SERVER['HTTPS'] = 'on';
    define('FORCE_SSL_ADMIN', true);
}


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
